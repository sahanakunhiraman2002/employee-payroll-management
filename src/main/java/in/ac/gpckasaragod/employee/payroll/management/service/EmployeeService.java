/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.service;

import in.ac.gpckasaragod.employee.payroll.management.model.ui.EmployeeUiModel;
import in.ac.gpckasaragod.employee.payroll.management.ui.model.data.Employee;
import java.util.List;

/**
 *
 * @author student
 */
public interface EmployeeService {

  
    public String saveEmployee(String name,String gender,Integer salaryId);
    public Employee readEmployee(Integer id);
    public List<EmployeeUiModel> getALLEmployee();
    public String updateEmployee(Integer id,String name,String gender,Integer salaryId);
    public String deleteEmployee(Integer id);
}
