/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Temp
lates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.ui.model.data;

/**
 *
 * @author student
 */
public class Salary {
    private String proffession;
    private Integer id;
    private Double netSalary;
    private Double tax;
    private Double ta;
    private Double da;
    private Double hra;
    private Double basicSalary;

    public Salary(String proffession, Integer id, Double netSalary, Double tax, Double ta, Double da, Double hra, Double basicSalary) {
        this.proffession = proffession;
        this.id = id;
        this.netSalary = netSalary;
        this.tax = tax;
        this.ta = ta;
        this.da = da;
        this.hra = hra;
        this.basicSalary = basicSalary;
    }

    /**
     *
     * @return 
     */
    @Override
    public String toString(){
    
        return proffession+"-"+netSalary+"-"+basicSalary;
    
}
    public String getProffession() {
        return proffession;
    }

    public void setProffession(String proffession) {
        this.proffession = proffession;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(Double netSalary) {
        this.netSalary = netSalary;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTa() {
        return ta;
    }

    public void setTa(Double ta) {
        this.ta = ta;
    }

    public Double getDa() {
        return da;
    }

    public void setDa(Double da) {
        this.da = da;
    }

    public Double getHra() {
        return hra;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(Double basicSalary) {
        this.basicSalary = basicSalary;
    }

   
}
