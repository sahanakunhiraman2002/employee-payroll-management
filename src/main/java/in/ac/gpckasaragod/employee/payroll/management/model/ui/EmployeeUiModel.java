/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.model.ui;

/**
 *
 * @author student
 */
public class EmployeeUiModel {
    private Integer id;
       private String  name;
       private String gender;
       private Integer salaryId;
       private String proffession;
       private Double netSalary;
       private Double tax;
       private Double ta;
       private Double da;
       private Double hra;
       private Double basicSalary;

    public EmployeeUiModel(Integer id, String name, String gender, Integer salaryId, String proffession, Double netSalary, Double tax, Double ta, Double da, Double hra, Double basicSalary) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.salaryId = salaryId;
        this.proffession = proffession;
        this.netSalary = netSalary;
        this.tax = tax;
        this.ta = ta;
        this.da = da;
        this.hra = hra;
        this.basicSalary = basicSalary;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(Integer salaryId) {
        this.salaryId = salaryId;
    }

    public String getProffession() {
        return proffession;
    }

    public void setProffession(String proffession) {
        this.proffession = proffession;
    }

    public Double getNetSalary() {
        return netSalary;
    }

    public void setNetSalary(Double netSalary) {
        this.netSalary = netSalary;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTa() {
        return ta;
    }

    public void setTa(Double ta) {
        this.ta = ta;
    }

    public Double getDa() {
        return da;
    }

    public void setDa(Double da) {
        this.da = da;
    }

    public Double getHra() {
        return hra;
    }

    public void setHra(Double hra) {
        this.hra = hra;
    }

    public Double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(Double basicSalary) {
        this.basicSalary = basicSalary;
    }
       

}
