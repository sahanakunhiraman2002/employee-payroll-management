/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.service;

import in.ac.gpckasaragod.employee.payroll.management.ui.model.data.Salary;
import java.util.List;

/**
 *
 * @author student
 */
public interface SalaryService {
    public String saveSalary(String proffession,Double tax,Double ta,Double da,Double hra,Double basicSalary);
    public Salary readSalary(Integer id);
    public List<Salary>getALLSalary();
    public String updateSalary(Integer id,String proffession,Double tax,Double ta,Double da,Double hra,Double basicSalary);
    public String deleteSalary(Integer id);

    
    

}