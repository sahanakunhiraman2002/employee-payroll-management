/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.service.impl;

import in.ac.gpckasaragod.employee.payroll.management.model.ui.EmployeeUiModel;
import in.ac.gpckasaragod.employee.payroll.management.service.EmployeeService;
import in.ac.gpckasaragod.employee.payroll.management.ui.model.data.Employee;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class EmployeeServiceImpl extends ConnectionServiceImpl implements EmployeeService {

    @Override
    public String saveEmployee(String name, String gender, Integer salaryId) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO EMPLOYEE (NAME,GENDER,SALARY_ID) VALUES"
                    + "('" + name + "','" + gender + "','" + salaryId + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";

            } else {
                return "Saved successfully";

            }

        } catch (SQLException ex) {
            Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";
        }

    }
      
    @Override
    public Employee readEmployee(Integer id) {
      // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     
            Employee employee;
            employee = null;
            try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM EMPLOYEE WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String gender = resultSet.getString("GENDER");
                Integer salaryId = resultSet.getInt("SALARY_ID");
                employee = new Employee(id, name, gender, salaryId);

            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
      return employee;
    }

    @Override
    public List<EmployeeUiModel> getALLEmployee() {
        List<EmployeeUiModel> salarys = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT  EMPLOYEE.ID,PROFFESION,TAX,DA,TA,HRA,BASIC_SALARY,NET_SALARY,NAME,GENDER,SALARY_ID FROM EMPLOYEE JOIN SALARY ON EMPLOYEE.SALARY_ID=SALARY.ID";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                
                Integer id = resultSet.getInt("ID");
                String proffession = resultSet.getString("PROFFESION");
                Double tax= resultSet.getDouble("TAX");
                Double netSalary = resultSet.getDouble("NET_SALARY");
                Double ta = resultSet.getDouble("TA");
                Double da = resultSet.getDouble("DA");
                Double hra = resultSet.getDouble("HRA");
                Double basicSalary = resultSet.getDouble("BASIC_SALARY");
                String name = resultSet.getString("NAME");
                String gender = resultSet.getString("GENDER");
                Integer salaryId = resultSet.getInt("SALARY_ID");
                EmployeeUiModel employeeUiModel =  new EmployeeUiModel(id,name,gender,salaryId,proffession,netSalary,tax,ta,da,hra,basicSalary);
               
                salarys.add(employeeUiModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return salarys;
    }
    

    @Override
    public String updateEmployee(Integer id, String name, String gender, Integer salaryId) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE EMPLOYEE SET NAME='" + name + "',GENDER='" + gender + "',SALARY_ID='" + salaryId + "' WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Updated successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Update failed";
    }

   
    @Override
   public String deleteEmployee(Integer id) {
      
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM EMPLOYEE WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete =statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
           return "Delete failed";
       }

    

    
   }

