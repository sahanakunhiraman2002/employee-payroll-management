/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.service.impl;

import in.ac.gpckasaragod.employee.payroll.management.service.SalaryService;
import in.ac.gpckasaragod.employee.payroll.management.ui.model.data.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class SalaryServiceImpl extends ConnectionServiceImpl implements SalaryService {

   @Override
   public String saveSalary(String proffession,Double tax,Double ta,Double da,Double hra,Double basicSalary) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement =connection.createStatement();
            Double netSalary=(basicSalary+ta+da+hra)- tax;
            String query ="INSERT INTO SALARY (PROFFESION,NET_SALARY,TAX,TA,DA,HRA,BASIC_SALARY) VALUES" 
                    +"('" + proffession + "','" + netSalary +"','" + tax +"','" +ta +"','" +da + "','" +hra +"','" + basicSalary + "')";
            System.err.println("Query:" +query);
             
            int status = statement.executeUpdate(query);
            if(status !=1) {
                return "save failed";
                
                
            } else {
                return "Saved successfully";
            }
            } catch (SQLException ex) {
            Logger.getLogger(SalaryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
              return "save failed";
            }
        
    }

    @Override
    public Salary readSalary(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          Salary  salary;
          salary = null;
          
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query ="SELECT * FROM SALARY WHERE ID=" +id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
            String proffession = resultSet.getString("PROFFESION");
            Double  netSalary=resultSet.getDouble("NET_SALARY");
            Double tax = resultSet.getDouble("TAX");
            Double ta = resultSet.getDouble("TA");
            Double da = resultSet.getDouble("DA");
            Double hra =resultSet.getDouble("HRA");
            Double basicSalary = resultSet.getDouble("BASIC_SALARY");
            salary = new Salary(proffession,id,netSalary,tax,ta,da,hra,basicSalary);
        }
            
         } catch (SQLException ex) {
            Logger.getLogger(SalaryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return salary;
          
    }

    @Override
    public List<Salary> getALLSalary() {
        List<Salary> salarys = new ArrayList<>();
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query= "SELECT *FROM SALARY";
            ResultSet resultSet=statement.executeQuery(query);
            while(resultSet.next()) {
                Integer id =resultSet.getInt("ID");
                String proffession = resultSet.getString("PROFFESION");
                
                Double tax= resultSet.getDouble("TAX");
                Double netSalary=resultSet.getDouble("NET_SALARY");
                Double ta = resultSet.getDouble("TA");
                Double da = resultSet.getDouble("DA");
                Double hra = resultSet.getDouble("HRA");
                Double basicSalary = resultSet.getDouble("BASIC_SALARY");
                
                Salary salary = new Salary(proffession,id,netSalary,tax,ta,da,hra,basicSalary);
                salarys.add(salary);
                
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SalaryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       return salarys;
    }

   @Override
     public String updateSalary(Integer id,String proffession,Double tax,Double ta,Double da,Double hra,Double basicSalary){
        try {
            // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Double netSalary=(basicSalary+ta+da+hra)- tax;
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query ="UPDATE SALARY SET PROFFESION='" + proffession + "',NET_SALARY='"+ netSalary +"',TAX='"+
                    tax +"',TA='" +ta +"',DA='" + da +"',HRA='" + hra +"',BASIC_SALARY='" + basicSalary + "'WHERE ID=" +id;
            System.out.print(query);
            int update =statement.executeUpdate(query);
            if (update !=1) {
              return "Update failed";
              
        }else {
                return "Update successfully";
                
            }
                            
        } catch (SQLException ex) {
            Logger.getLogger(SalaryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Update failed";
    }

    @Override
    public String deleteSalary(Integer id) {
        try {
            // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection =getConnection();
            String query ="DELETE FROM SALARY WHERE ID =?";
            PreparedStatement statement =connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1) 
                return "Deleted failed";
            else
                return"Deleted successfully";
            
        } catch (SQLException ex) {
            Logger.getLogger(SalaryServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
          return "Deleted failed";  
    }

    

 
 
    
}
