/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.employee.payroll.management.ui.model.data;

/**
 *
 * @author student
 */
public class Employee { 
    private Integer id;
       private String  name;
       private String gender;
       private Integer salaryId;

    public Employee(Integer id, String name, String gender, Integer salaryId) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.salaryId = salaryId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(Integer salaryId) {
        this.salaryId = salaryId;
    }

    
       
    
}
